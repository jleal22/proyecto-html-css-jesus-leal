### Bootcamp Full Stack developer - Upgrade Hub
## Proyecto HTML y CSS
---

Página Web creada utilizando HTML5 y SCSS (CSS) como proyecto del primer módulo del bootcamp.

La idea es crear una web donde se presenta el portafolio de trabajados realizados como desarrollador.

* Visitar la web, disponible en:
[https://jesusleal.w3spaces.com/](https://jesusleal.w3spaces.com/)

* Todo el código fuente está disponible en el repositorio:
[https://gitlab.com/jleal22/proyecto-html-css-jesus-leal](https://gitlab.com/jleal22/proyecto-html-css-jesus-leal)

![View web](./images/captura.jpg)
...

**Estructura:**

Componentes creados:
- index.html
- pages/contact.html
- pages/portfolio.html
- styles/styles.scss
- styles/style.css (Generado a partir de styles.scss)
